const { Client, xml } = require('xmpp.js');

const client = new Client();

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const chatMessage = xml('message', { type: 'chat' });

client.on('error', err => console.log('ERROR:', err.toString));

client.on('status', status => console.log('STATUS:', status));

client.on('input', input => console.log('INPUT:', input));

client.on('output', output => console.log('OUTPUT:', output));

client.on('stanza', stanza => {
  console.log('STANZA:', JSON.stringify(stanza.toJSON()));

  if (stanza.is('presence') && stanza.attrs.type === 'subscribe') {
    client.send(
      xml('presence', { to: stanza.attrs.from, type: 'subscribed' })
    );
  }

  if (stanza.is('message') && stanza.attrs.from !== client.jid) {
    const body = stanza.getChild('body').text();
    const response = chatMessage;
    response.attrs.to = stanza.attrs.from;
    response.append(xml('body', {}, body));
    client.send(response);
  }
});

client.on('online', jid => {
  console.log('ONLINE:', jid.toString());
  client.send(
    xml('presence', {}, 
      xml('show', {}, 'chat'),
      xml('status', {}, 'I say everything you do!'),
    )
  );
});

client.handle('authenticate', authenticate => {
  return authenticate('echo_bot', 'password');
});

client
  .start('xmpp://localhost:5222')
  .catch(err => console.error('start failed', err.message));
