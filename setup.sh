apt-get update

apt-get install -y ejabberd

sed -i 's/    ip: "::"/    ip: "0.0.0.0"/g' /etc/ejabberd/ejabberd.yml
sed -i 's/         - "": "localhost"/         - "admin": "localhost"/' /etc/ejabberd/ejabberd.yml

systemctl restart ejabberd

sleep 5

ejabberdctl register admin localhost password
ejabberdctl register echo_bot localhost password
